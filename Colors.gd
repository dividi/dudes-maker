extends Control

# palettes are here : https://lospec.com/


var palettes := {
	"Resurrect64" : ["2e222f","3e3546","625565","966c6c","ab947a","694f62","7f708a","9babb2","c7dcd0","ffffff","6e2727","b33831","ea4f36","f57d4a","ae2334","e83b3b","fb6b1d","f79617","f9c22b","7a3045","9e4539","cd683d","e6904e","fbb954","4c3e24","676633","a2a947","d5e04b","fbff86","165a4c","239063","1ebc73","91db69","cddf6c","313638","374e4a","547e64","92a984","b2ba90","0b5e65","0b8a8f","0eaf9b","30e1b9","8ff8e2","323353","484a77","4d65b4","4d9be6","8fd3ff","45293f","6b3e75","905ea9","a884f3","eaaded","753c54","a24b6f","cf657f","ed8099","831c5d","c32454","f04f78","f68181","fca790","fdcbb0"],
	"on70": ["690653","7d1059","8f1d58","a32c5c","b33d60","c24f64","d26471","632121","702d2d","7d3939","8c4949","995959","a66868","b37979","57210f","663217","754924","856232","967d45","a38e55","b0a568","1d1e61","2a3270","2f3f75","3d5b85","4b7296","608ea8","75a9ba","2d611a","327026","398231","3d8f40","53a35f","5ea86e","72b887"],
	"Moorland46":["211021","291b25","2e2228","3b2d2f","473a3a","4f4744","63584f","786e55","848565","62694c","485939","335221","154715","12381b","142e2e","08171f","0f1b29","191d36","26203d","382f4a","3f3e52","454859","4e5970","535e8a","687299","6e87b8","8d95cc","9db1d9","adb9de","b5cdeb","b8d7f2","cfeefc","e9f0d1","ecf0d3","e8d6a2","e0b57f","d99853","bd7748","a14b23","91301f","732929","612838","4d233e","361a36","261a36","130f26"],
	"Endesga32":["be4a2f","d77643","ead4aa","e4a672","b86f50","733e39","3e2731","a22633","e43b44","f77622","feae34","fee761","63c74d","3e8948","265c42","193c3e","124e89","0099db","2ce8f5","ffffff","c0cbdc","8b9bb4","5a6988","3a4466","262b44","181425","ff0044","68386c","b55088","f6757a","e8b796","c28569"],
	"AAP-64":["060608","141013","3b1725","73172d","b4202a","df3e23","fa6a0a","f9a31b","ffd541","fffc40","d6f264","9cdb43","59c135","14a02e","1a7a3e","24523b","122020","143464","285cc4","249fde","20d6c7","a6fcdb","ffffff","fef3c0","fad6b8","f5a097","e86a73","bc4a9b","793a80","403353","242234","221c1a","322b28","71413b","bb7547","dba463","f4d29c","dae0ea","b3b9d1","8b93af","6d758d","4a5462","333941","422433","5b3138","8e5252","ba756a","e9b5a3","e3e6ff","b9bffb","849be4","588dbe","477d85","23674e","328464","5daf8d","92dcba","cdf7e2","e4d2aa","c7b08b","a08662","796755","5a4e44","423934"],
	"Pear36":["5e315b","8c3f5d","ba6156","f2a65e","ffe478","cfff70","8fde5d","3ca370","3d6e70","323e4f","322947","473b78","4b5bab","4da6ff","66ffe3","ffffeb","c2c2d1","7e7e8f","606070","43434f","272736","3e2347","57294b","964253","e36956","ffb570","ff9166","eb564b","b0305c","73275c","422445","5a265e","80366b","bd4882","ff6b97","ffb5b5"],
	"Zughy32":["472d3c","5e3643","7a444a","a05b53","bf7958","eea160","f4cca1","b6d53c","71aa34","397b44","3c5956","302c2e","5a5353","7d7071","a0938e","cfc6b8","dff6f5","8aebf1","28ccdf","3978a8","394778","39314b","564064","8e478c","cd6093","ffaeb6","f4b41b","f47e1b","e6482e","a93b3b","827094","4f546b"],
	"Journey":["050914","110524","3b063a","691749","9c3247","d46453","f5a15d","ffcf8e","ff7a7d","ff417d","d61a88","94007a","42004e","220029","100726","25082c","3d1132","73263d","bd4035","ed7b39","ffb84a","fff540","c6d831","77b02a","429058","2c645e","153c4a","052137","0e0421","0c0b42","032769","144491","488bd4","78d7ff","b0fff1","faffff","c7d4e1","928fb8","5b537d","392946","24142c","0e0f2c","132243","1a466b","10908e","28c074","3dff6e","f8ffb8","f0c297","cf968c","8f5765","52294b","0f022e","35003b","64004c","9b0e3e","d41e3c","ed4c40","ff9757","d4662f","9c341a","691b22","450c28","2d002e"],
	"Apollo":["172038","253a5e","3c5e8b","4f8fba","73bed3","a4dddb","19332d","25562e","468232","75a743","a8ca58","d0da91","4d2b32","7a4841","ad7757","c09473","d7b594","e7d5b3","341c27","602c2c","884b2b","be772b","de9e41","e8c170","241527","411d31","752438","a53030","cf573c","da863e","1e1d39","402751","7a367b","a23e8c","c65197","df84a5","090a14","10141f","151d28","202e37","394a50","577277","819796","a8b5b2","c7cfcc","ebede9"],
	"Lospec500":["10121c","2c1e31","6b2643","ac2847","ec273f","94493a","de5d3a","e98537","f3a833","4d3533","6e4c30","a26d3f","ce9248","dab163","e8d282","f7f3b7","1e4044","006554","26854c","5ab552","9de64e","008b8b","62a477","a6cb96","d3eed3","3e3b65","3859b3","3388de","36c5f4","6dead6","5e5b8c","8c78a5","b0a7b8","deceed","9a4d76","c878af","cc99ff","fa6e79","ffa2ac","ffd1d5","f6e8e0","ffffff"]
}

var palette = "Resurrect64"


func _ready():

	# default palette
	load_palette(palette)

	# populate palettes list
	for i in palettes.keys():
		$PalettesList.add_item(i)
	

# select a new palette
func _on_PalettesList_item_selected(index):
	palette = $PalettesList.get_item_text(index)
	load_palette(palette)


# clear all colors in palette
func clear_palette():
	for c in $Palette.get_children():
		c.queue_free()


# load a palette
func load_palette(palette_name):

	clear_palette()
	
	var colors = palettes.get(palette_name)	
	var white_tex = preload("res://assets/pixel-white.png")
	
	for i in colors:
		var button = TextureButton.new()
		button.connect("pressed", get_parent(), "_on_ColorPreset_pressed", [i])
		button.texture_normal = white_tex
		button.expand = true
		button.rect_min_size = Vector2(16,16)
		button.self_modulate = Color(i)
		button.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
		$Palette.add_child(button)


	
	
func get_random_color():
	randomize()
	var current_palette = palettes.get(palette)
	return current_palette[randi() % current_palette.size()]
