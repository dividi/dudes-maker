--[[
   A script to save all different layers in different .png files
--]]


local folder = "C:\\Users\\dividi\\Documents\\MEGAsync\\__DEV\\games\\godot\\Dudes Maker\\assets\\parts\\"

local sprite = app.activeSprite
local layers = sprite.layers

-- hide all layers (even groups)
print ("Hide all layers")
for i, layer in ipairs(layers) do
   if layer.isGroup then
      layer.isVisible = true
   else
      layer.isVisible = false
   end
end

-- show a layer, save it, hide it again
for i, layer in ipairs(layers) do
   
   if (not layer.isGroup) then
      
      layer.isVisible = true
      
      local my_parent = string.lower(layer.parent.name)
      print ("put " .. layer.name .. " in " .. my_parent .. " folder")
            
      sprite:saveCopyAs(folder .. my_parent .. "\\" .. layer.name .. ".png")
      --sprite:saveCopyAs(folder .. layer.name .. ".png")

      layer.isVisible = false

   end

end

print ("END")
