extends Node2D

export var animation_speed := 1.5

var parts = ["base","bottom", "hand", "eyes", "top", "face"]

func _ready():
	init_animation()
	$AnimationPlayer.playback_speed = animation_speed
	
	# init all shaders
	for part in parts :
		get_node(part).material.set("shader_param/OLDCOLOR1", Color("#808080"))
		get_node(part).material.set("shader_param/OLDCOLOR2", Color("#FFFFFF"))
		get_node(part).material.set("shader_param/NEWCOLOR1", Color("#808080"))
		get_node(part).material.set("shader_param/NEWCOLOR2", Color("#FFFFFF"))



func change_part (part, value):
	if value == 0:
		get_node(part).texture = null
	else:
		get_node(part).texture = load(str("res://assets/parts/", part.to_lower(), "/", part.to_lower(), value, ".png"))



func change_animation(animation_name):
	if animation_name == "Stop":
		$AnimationPlayer.stop()
	else:
		$AnimationPlayer.play(animation_name)
	

func change_color1 (part, color):
	get_node(part).material.set("shader_param/OLDCOLOR1", Color("#808080"))
	get_node(part).material.set("shader_param/NEWCOLOR1", color)
	

func change_color2 (part, color):
	get_node(part).material.set("shader_param/OLDCOLOR2", Color("#FFFFFF"))
	get_node(part).material.set("shader_param/NEWCOLOR2", color)


func init_animation():

	var anims = {
		"animations":[
			{"name":"Idle", "start":0, "end":3, "speed":1.5}, 
			{"name":"Run", "start":6, "end":11, "speed":1.5},
			{"name":"Jump", "start":12, "end":17, "speed":1.5}
		]
	}
	

	for anim in anims.animations:

		var animation = Animation.new()

		animation.length = 1.0
		animation.loop = true

		$AnimationPlayer.add_animation(anim.name, animation)
		
		for part in parts :	
			var track = animation.add_track(Animation.TYPE_VALUE)
			animation.track_set_path(track, str(part, ":frame"))
			animation.track_insert_key(track, 0.0, anim.start)
			animation.track_insert_key(track, 1.0, anim.end)



	$AnimationPlayer.current_animation = "Idle"
	
