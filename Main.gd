extends Control

var current_color = 1 # 1 for primary, 2 for secondary. not a bool because maybe one day a third color

func _ready():
	
	$ChangeParts/ChangeTop.max_value = get_files_count("top")
	$ChangeParts/ChangeEyes.max_value = get_files_count("eyes")
	$ChangeParts/ChangeFace.max_value = get_files_count("face")
	$ChangeParts/ChangeBottom.max_value = get_files_count("bottom")
	$ChangeParts/ChangeHand.max_value = get_files_count("hand")
	$ChangeParts/ChangeBase.max_value = get_files_count("base")
	
	# init shader for the first item in the list (base)
	change_color_selector($PrimaryColor, $Dude/base.material.get_shader_param("NEWCOLOR1"))
	change_color_selector($SecondaryColor, $Dude/base.material.get_shader_param("NEWCOLOR2"))


# get the count of assets in a specific folder
func get_files_count(part):
	var dir = Directory.new()
	var path = str("res://assets/parts/", part)
	var count = 0
	if dir.open(path) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if not dir.current_is_dir() and not file_name.match("*.import"):
				count += 1
				
			file_name = dir.get_next()
		
		return count

	else:
		print("An error occurred when trying to access the path.")
		return 0


# generate a random dude
func _on_GenerateRandom_pressed():
	randomize()
	
	$ChangeParts/ChangeBase.value = randi() % int($ChangeParts/ChangeBase.max_value) + 1	# avoid index = 0 : "no body"
	$ChangeParts/ChangeTop.value = randi() % int($ChangeParts/ChangeTop.max_value)
	$ChangeParts/ChangeEyes.value = randi() % int($ChangeParts/ChangeEyes.max_value)
	$ChangeParts/ChangeFace.value = randi() % int($ChangeParts/ChangeFace.max_value)
	$ChangeParts/ChangeBottom.value = randi() % int($ChangeParts/ChangeBottom.max_value)
	# hand should'nt be randomized


# select animation
func _on_AnimationList_item_selected(index):
	var name = $AnimationList.get_item_text(index)
	$Dude.change_animation(name)



# change shader color

func _on_ColorPicker_color_changed(color):
	var part_name = $PartList.get_item_text($PartList.selected)
	update_color(color, part_name, current_color)



# on click on a color preset
func _on_ColorPreset_pressed (c):
	
	var color = Color(c) # convert hex -> color
	var part_name = $PartList.get_item_text($PartList.selected)
	update_color(color, part_name, current_color)


# change color on a part
func update_color(color, part_name, which_one):
	if which_one == 1:
		$Viewport/Dude.change_color1(part_name, color)
		$Dude.change_color1(part_name, color)
		change_color_selector($PrimaryColor, color)
	else:
		$Viewport/Dude.change_color2(part_name, color)
		$Dude.change_color2(part_name, color)
		change_color_selector($SecondaryColor, color)
	

# choose primary color
func _on_PrimaryColor_pressed():
	current_color = 1

# choose secondary color
func _on_SecondaryColor_pressed():
	current_color = 2


# select a part to change the color on
func _on_PartList_item_selected(index):
	var part = $PartList.get_item_text(index)
	change_color_selector($PrimaryColor, $Dude.get_node(part).material.get_shader_param("NEWCOLOR1"))
	change_color_selector($SecondaryColor, $Dude.get_node(part).material.get_shader_param("NEWCOLOR2"))




# change parts

func _on_ChangePart(value, part):
	$Viewport/Dude.change_part(part, value)
	$Dude.change_part(part, value)

	match(part):
		"base":$PartList.select(0)
		"top":$PartList.select(1)
		"face":$PartList.select(2)
		"bottom":$PartList.select(3)
		"hand":$PartList.select(4)
		"eyes":$PartList.select(5)
	
	# update color boxes
	change_color_selector($PrimaryColor, $Dude.get_node(part).material.get_shader_param("NEWCOLOR1"))
	change_color_selector($SecondaryColor, $Dude.get_node(part).material.get_shader_param("NEWCOLOR2"))




func change_color_selector(selector, color):
	selector.get("custom_styles/normal").bg_color = color
	selector.get("custom_styles/pressed").bg_color = color
	selector.get("custom_styles/focus").bg_color = color
	selector.get("custom_styles/hover").bg_color = color


# randomize a color on the current part
func _on_RandomColors1_pressed():
	randomize()
	var color = Color($Palette.get_random_color())
	var part_name = $PartList.get_item_text($PartList.selected)
	
	update_color(color, part_name, current_color)


# randomize all colors on the current part
func _on_RandomColors2_pressed():
	randomize()
	var color1 = Color($Palette.get_random_color())
	var color2 = Color($Palette.get_random_color())
	var part_name = $PartList.get_item_text($PartList.selected)
	
	update_color(color1, part_name, 1)
	update_color(color2, part_name, 2)


# randomize all colors on all parts
func _on_RandomColors3_pressed():
	randomize()
	
	for part_name in $Dude.parts:
		var color1 = Color($Palette.get_random_color())
		var color2 = Color($Palette.get_random_color())
				
		update_color(color1, part_name, 1)
		update_color(color2, part_name, 2)	


# change zoom factor
func _on_ZoomFactor_item_selected(index):
	var factor = int($ZoomFactor.get_item_text(index).replace("x", ""))
	$Dude.scale = Vector2(factor, factor)




# export sprite shit (natalia gonna kick my a**)

#good res here : https://gamedev.stackexchange.com/questions/197017/how-can-i-bake-2d-sprites-in-godot-at-runtime
func _on_ExportSpriteSheet_pressed():
	$FileDialogBack.show()
	$FileDialog.show()
	
func export_spritesheet(filename):
	
	# stop player 
	$Viewport/Dude/AnimationPlayer.stop()
	
	# create the sheet image
	var sheet = Image.new() 
	sheet.create(144,72,false,Image.FORMAT_RGBA8)
			
	for i in range(0,18):
				
		$Viewport/Dude/base.frame = i
		$Viewport/Dude/top.frame = i
		$Viewport/Dude/bottom.frame = i
		$Viewport/Dude/eyes.frame = i
		$Viewport/Dude/hand.frame = i
		$Viewport/Dude/face.frame = i
		
		var viewport = $Viewport
		
		yield(VisualServer, "frame_post_draw")	# wait for the frame
		
		var image = viewport.get_texture().get_data()
		
		var coords = $Viewport/Dude/base.frame_coords * 24
		
		sheet.blit_rect(image, Rect2(Vector2.ZERO, image.get_size()), coords)
	
	# save the sprite sheet
	sheet.save_png(filename)
	



func _on_FileDialog_confirmed():
	$FileDialogBack.hide()
	export_spritesheet($FileDialog.current_path)
	
